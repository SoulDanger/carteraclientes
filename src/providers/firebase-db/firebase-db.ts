import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the FirebaseDbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseDbProvider {

  constructor(public afDB: AngularFireDatabase,
  			  public auth: AuthProvider) {
    console.log('Hello FirebaseDbProvider Provider');
  }

  	//Te guarda un cliente en la base de datos online.
	guardaCliente(cliente){
		if(!cliente.id) {
	    	cliente.id  = Date.now();
		}
	    return this.afDB.database.ref('Usuarios/'+this.auth.getUser() + '/Clientes/' +  cliente.id).set(cliente)
	}

	//Te retorna nos clientes que tiene agregado cada cuenta.
	getClientes(){
	    return this.afDB.list('Usuarios/'+this.auth.getUser()+'/Clientes').valueChanges();
	}

	//Borra un cliente de la base de datos.
	public borrarCliente(id){
        this.afDB.database.ref('Usuarios/'+this.auth.getUser()+'/Clientes/'+id).remove();
	}

}

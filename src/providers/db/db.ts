import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
/*
  Generated class for the DbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DbProvider {

	db : SQLiteObject = null;

  constructor(public sqlite: SQLite) {
    console.log('Hello DbProvider Provider');
  }

  public openDb(){
      return this.sqlite.create({
          name: 'data.db',
          location: 'default' // el campo location es obligatorio
      })
      .then((db: SQLiteObject) => {
       this.db = db;
     })
  }

  public createTableClientes(){
    return this.db.executeSql("create table if not exists clientes( id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, apellidos TEXT, direccion TEXT, poblacion TEXT, email TEXT, telefono TEXT, dni TEXT, preferencias TEXT, foto TEXT)",{})
  }

  public addCliente(cliente){
    let sql = "INSERT INTO clientes (nombre, apellidos, direccion, poblacion, email, telefono, dni, preferencias, foto) values (?,?,?,?,?,?,?,?,?)";
    return this.db.executeSql(sql,[cliente.nombre, 
                                  cliente.apellidos, 
                                  cliente.direccion, 
                                  cliente.poblacion,
                                  cliente.email, 
                                  cliente.telefono, 
                                  cliente.dni, 
                                  cliente.preferencias, 
                                  cliente.foto
                                  ]);
  }

  public getClientes(){
    let sql = "SELECT * FROM clientes";
    return this.db.executeSql(sql,{});
  }

  public editCliente(cliente){

    let sql = "UPDATE clientes SET nombre = ?, apellidos = ?, direccion = ?, poblacion = ?, email = ?, telefono = ?, dni = ?, preferencias = ?, foto = ? WHERE id = ?";
    return this.db.executeSql(sql,[cliente.nombre, 
                                  cliente.apellidos, 
                                  cliente.direccion, 
                                  cliente.poblacion,
                                  cliente.email, 
                                  cliente.telefono, 
                                  cliente.dni, 
                                  cliente.preferencias, 
                                  cliente.foto, 
                                  cliente.id
                                  ]);
    
  }


  public deleteCliente(id){

    let sql = "DELETE FROM clientes WHERE id = ?";

    return this.db.executeSql(sql,[id]);

  }

}


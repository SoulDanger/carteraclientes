import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { InicioPage } from '../inicio/inicio';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	user= { email: '', password: ''};

  constructor(public navCtrl: NavController,
  			  public navParams: NavParams,
  			  public auth: AuthProvider,
  			  public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  	//recoge los datos y te añade un nuevo usuario a la base de datos.
	signin(){
	    this.auth.registerUser(this.user.email,this.user.password)
	    .then((user) => {
	      // El usuario se ha creado correctamente
	    })
	    .catch(err=>{
	      let alert = this.alertCtrl.create({
	        title: 'Error',
	        subTitle: err.message,
	        buttons: ['Aceptar']
	      });
	      alert.present();
	    })
	}

	//recoge los datos y los compara en la base de datos, si existen iniciara session, si no muestra error.
	login(){
	    this.auth.loginUser(this.user.email,this.user.password ).then((user) => {
	      }
	    )
	     .catch(err=>{
	      let alert = this.alertCtrl.create({
	        title: 'Error',
	        subTitle: err.message,
	        buttons: ['Aceptar']
	      });
	      alert.present();
	    })
  }


}

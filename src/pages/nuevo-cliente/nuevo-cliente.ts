import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
//import { DbProvider } from '../../providers/db/db';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';


/**
 * Generated class for the NuevoClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nuevo-cliente',
  templateUrl: 'nuevo-cliente.html',
})
export class NuevoClientePage {

  nombre: string;
  apellidos: string;
  direccion: string;
  poblacion: string;
  email: string;
  telefono: string;
  dni: string;
  preferencias: string = '';
  foto: any = '';

  formRegistro: FormGroup;

  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  private camera: Camera,
  			  //private db: DbProvider,
          public fb: FormBuilder,
          private dbFirebase: FirebaseDbProvider) {

    this.formRegistro = this.fb.group({
      //Te valida que los datos sean correctos antes de guardarlos.
      nombre: ['',[Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      apellidos: ['',[Validators.required, Validators.maxLength(50)]],
      direccion: ['',[ Validators.required, Validators.maxLength(40)]],
      poblacion: ['',[Validators.required, Validators.maxLength(40)]],
      email: ['',[Validators.required, Validators.email]],
      telefono: ['',[Validators.required, Validators.pattern(/^[0-9]{9}$/)]],
      dni: ['',[Validators.required, Validators.pattern(/^[0-9]{8}[-][A-Z]{1}$/)]],
      preferencias: ['',[Validators.maxLength(200)]],

    });

  }

  //Cancela el registro y vuelve a la pagina de la que has venido.
  cancelarRegistro() {

    this.navCtrl.pop();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevoClientePage');
  }


  sacarFoto(){

    let cameraOptions : CameraOptions = {
        quality: 50,
        encodingType: this.camera.EncodingType.JPEG, 
        targetWidth: 800,
        targetHeight: 600,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.CAMERA,
        correctOrientation: true
    }


    this.camera.getPicture(cameraOptions).then((imageData) => {
      // imageData is a base64 encoded string
        this.foto = "data:image/jpeg;base64," + imageData;
    }, (err) => {
        console.log(err);
    });
  }

  //Te recoge los datos y te los añade a la base de datos.
  guardarCliente(){
    let cliente = {
      nombre: this.nombre,
      apellidos: this.apellidos,
      direccion: this.direccion,
      poblacion: this.poblacion,
      email: this.email,
      telefono: this.telefono,
      dni: this.dni,
      preferencias: this.preferencias,
      foto: this.foto
    }

    this.dbFirebase.guardaCliente(cliente).then(res=>{
      console.log('Cliente guardado en firebase:');
      this.navCtrl.pop();
    })//,(err)=>{ alert('error al meter en la bd'+err) })

  }


}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { NuevoClientePage } from '../nuevo-cliente/nuevo-cliente';
//import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/auth/auth';
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';

/**
 * Generated class for the InicioPage page.
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html'
})
export class InicioPage {

  clientes: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public modalCtrl: ModalController,
              //public db: DbProvider,
              public alertCtrl: AlertController,
              public auth: AuthProvider,
              public dbFirebase: FirebaseDbProvider) {
  }

  nuevoCliente() {

    this.navCtrl.push(NuevoClientePage);
    
  }

  //Cuando seleccionas un cliente, te abre un modal que te muestra los detalles de ese objeto, le pasas como parametro el objeto.
  detallesCliente(cliente){
      let modalDetalleCliente = this.modalCtrl.create( 'ModalEditarClientePage', cliente );
      modalDetalleCliente.present();
     }

  //Le pasas el id como parametro, y te borra el objeto entero si le das a si, si le das a no no hace ningun cambio.
  borrarCliente(id) {

    let alert = this.alertCtrl.create({

      title: 'Confirmar borrado',
      message: '¿Estás seguro de que deseas eliminar este cliente?',
      buttons: [

        {
          text: 'no',
          role: 'cancel',
          handler: () => {

          }
        },

        {
          text: 'Si',
          handler: () => {

            //Aqui borramos el cliente en firebase
            this.dbFirebase.borrarCliente(id);

          }
        }

      ]

    });
    //Llama al alert.
    alert.present();

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad InicioPage');
  }

  //Cuando entra a la pagina(sea activa) coge los datos de la db y los muestra.
  ionViewWillEnter(){

    this.dbFirebase.getClientes().subscribe(clientes=>{
      this.clientes = clientes;

    },(err)=>{ /* alert('error al sacar de la bd'+err) */ })
 
  }

  //Para cerrar la sesion.
  cerrarSesion(){
    this.auth.logout();
  }


}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
//import { DbProvider } from '../../providers/db/db';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';

/**
 * Generated class for the ModalEditarClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-editar-cliente',
  templateUrl: 'modal-editar-cliente.html',
})
export class ModalEditarClientePage {

	cliente: any;
  edit: boolean = false;

  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  private viewCtrl: ViewController,
          private camera: Camera,
          //private db: DbProvider,
          private LaunchNavigator: LaunchNavigator,
          private dbFirebase: FirebaseDbProvider) {

  	this.cliente = this.navParams.data;
  	
  }

  //Cierra el modal de detalles.
  cerrarModalDetalles(){

    this.viewCtrl.dismiss();

  }

  //Cambia de true a false o viceversa para mostrar una pagina o otra.
  editar() {

    this.edit = true;

  }

  //Recoge la foto sacada por la camara.
  sacarFoto(){

    let cameraOptions : CameraOptions = {
        quality: 50,
        encodingType: this.camera.EncodingType.JPEG,
        targetWidth: 800,
        targetHeight: 600,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.CAMERA,
        correctOrientation: true
    }


    this.camera.getPicture(cameraOptions).then((imageData) => {
      // imageData is a base64 encoded string
        this.cliente.foto = "data:image/jpeg;base64," + imageData;
    }, (err) => {
        console.log(err);
    });
  }

  //Guarda los cambios recogidos del formulario y te actualiza todos los campos del objeto.
  guardarCambios(){

     let cliente = {
      id : this.cliente.id,
      nombre: this.cliente.nombre,
      apellidos: this.cliente.apellidos,
      direccion: this.cliente.direccion,
      poblacion: this.cliente.poblacion,
      email: this.cliente.email,
      telefono: this.cliente.telefono,
      dni: this.cliente.dni,
      preferencias: this.cliente.preferencias,
      foto: this.cliente.foto
    }

    this.dbFirebase.guardaCliente(cliente).then(res=>{
        console.log('Cliente modificado en firebase');
        this.cerrarModalDetalles();
    })
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalEditarClientePage');
  }

    //Te abre google maps recogiendote los campos de direccion y poblacion y te los muestra en la aplicacion para saber como llegar a esa direccion.
    comoLlegar(){
    let destino = this.cliente.direccion +', '+ this.cliente.poblacion;
    this.LaunchNavigator.navigate(destino)
    .then(
      success => console.log('Launched navigator'),
      error => console.log('Error launching navigator', error)
    );

 }

}

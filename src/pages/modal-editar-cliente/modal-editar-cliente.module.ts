import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEditarClientePage } from './modal-editar-cliente';

@NgModule({
  declarations: [
    ModalEditarClientePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEditarClientePage),
  ],
})
export class ModalEditarClientePageModule {}

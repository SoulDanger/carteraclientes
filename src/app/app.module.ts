import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { MyApp } from './app.component';
import { NuevoClientePage } from '../pages/nuevo-cliente/nuevo-cliente';
import { DbProvider } from '../providers/db/db';
import { SQLite } from '@ionic-native/sqlite';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { FirebaseDbProvider } from '../providers/firebase-db/firebase-db';

export const firebaseConfig = {
  apiKey: "AIzaSyBUUy4lkLjxNAYup6zodF8tAmJbi93sT4M",
  authDomain: "carteraclientes-6cfa0.firebaseapp.com",
  databaseURL: "https://carteraclientes-6cfa0.firebaseio.com",
  projectId: "carteraclientes-6cfa0",
  storageBucket: "carteraclientes-6cfa0.appspot.com",
  messagingSenderId: "958479957431"
};

@NgModule({
  declarations: [
    MyApp,
    NuevoClientePage  
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NuevoClientePage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation, 
    Camera,
    SQLite,
    DbProvider,
    LaunchNavigator,
    AuthProvider,
    FirebaseDbProvider
  ]
})
export class AppModule {}
